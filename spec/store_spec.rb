require 'time'
require './store.rb'
require './incident.rb'
require './datetime_helper'
describe Store do
  describe '.initialize' do
    let!(:incidents) { (1..5).map{|i| Incident.new("Test Incident #{i}") }}
    let!(:store) { Store.new(incidents) }
    let!(:today) { Time.now.strftime("%Y-%m-%d") }
    context "given an array of incidents for store initialization" do
      it "returns total size of incidents created" do
        expect(store.incidents.size).to eq(5)
      end
    end
    context "given an array of incidents without created_at dates for store initialization" do
      it "returns today date" do
        store.incidents.each do |incident|
          expect(incident.created_at).to eq(today)
        end
      end
    end
  end
  describe '.set_stats' do
    let!(:stats) { {:open_cases=> 0, :closed_cases => 0, :average_solution => 0, :maximum_solution => 0 }}
    let!(:store_new_stats) { Store.new.send(:set_stats) }

    context "when is called set_stats" do
      it "returns an hash size as 4" do
        expect(stats.keys.size).to eq(4)
      end
    end
    context "when is called set_stats" do
      it "returns an hash with four keys and values in zero" do
        expect(store_new_stats).to eq(stats)
      end
    end
  end
  describe '.filtered_cases' do
    let!(:filtered_cases) { {:open_cases => nil, :closed_cases => nil } }
    let!(:store_new_filtered_cases) { Store.new.send(:filtered_cases) }

    context "when is called set_stats" do
      it "returns an hash size as 4" do
        expect(store_new_filtered_cases.keys.size).to eq(filtered_cases.keys.size)
      end
    end
    context "when is called set_stats" do
      it "returns an hash with four keys and values in zero" do
        expect(store_new_filtered_cases).to eq(filtered_cases)
      end
    end
  end
  describe '.incident_status' do
    let!(:start_date) { '2021-05-01' }
    let!(:end_date) { Time.now.strftime("%Y-%m-%d") }
    let!(:incidents) do
      t = 1.upto(100).map do |id|
        st_date = DateTimeHelper.convert_date_to_unixtime('2021-05-01')
        factor = ((DateTimeHelper::DAY_IN_SECONDS * id) + DateTimeHelper::DAY_IN_SECONDS)
        st_date += factor
        Incident.new("Incident_#{id}", DateTimeHelper.convert_unixtime_to_date(st_date))
      end
      return t
    end
    let!(:store) do
      incidents[49..85].each(&:mark_as_solved)
      Store.new(incidents)
    end
    let!(:mock_stats){ {:open_cases=>63, :closed_cases=>37, :average_solution=>178, :maximum_solution=>196} }
    let!(:stats) { store.incident_status(start_date, end_date) }

    context 'when some incidents are marked as solved' do
      it 'returns a hash with results' do
        expect(stats).to eq(mock_stats)
      end
    end
    context 'when the calculation is executed' do
      it 'returns all values' do
        stats.keys.each do |st|
          expect(stats[st]).to eq(mock_stats[st])
        end
      end
    end
  end
end