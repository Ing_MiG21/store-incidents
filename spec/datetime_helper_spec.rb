require './datetime_helper'

describe DateTimeHelper do
  describe '.calculate_time_between_dates' do
    let!(:start_date) { '2020-01-01' }
    let!(:end_date) { '2020-01-02' }

    context 'when two dates are substracted' do
      it 'returns difference greater than zero in seconds' do
        value = DateTimeHelper.calculate_time_between_dates(start_date, end_date, convert_into = 'seconds')
        expect(value).not_to eq(0)
      end
      it 'returns exact seconds from a day difference' do
        value = DateTimeHelper.calculate_time_between_dates(start_date, end_date, convert_into = 'seconds')
        expect(value).to eq(86400)
      end
      it 'returns difference greater than zero in days' do
        value = DateTimeHelper.calculate_time_between_dates(start_date, end_date, convert_into = 'days')
        expect(value).not_to eq(0)
      end
      it 'returns exact days from a day difference' do
        value = DateTimeHelper.calculate_time_between_dates(start_date, end_date, convert_into = 'days')
        expect(value).to eq(1)
      end
      it 'returns difference greater than zero in hours' do
        value = DateTimeHelper.calculate_time_between_dates(start_date, end_date, convert_into = 'hours')
        expect(value).not_to eq(0)
      end
      it 'returns exact hours from a day difference' do
        value = DateTimeHelper.calculate_time_between_dates(start_date, end_date, convert_into = 'hours')
        expect(value).to eq(24)
      end
    end
  end
  #
  describe '.convert_date_to_unixtime' do
    let!(:mock_unixtime) { 1609470000 }
    let!(:real_unixtime) { DateTimeHelper.convert_date_to_unixtime('2021-01-01') }
    context 'when convert date from Y-m-d format to unixtime' do
      it 'returns an integer as unixtime result' do
        expect(real_unixtime).to eq(mock_unixtime)
      end
    end
  end

  describe '.convert_unixtime_to_date' do
    let!(:mock_datetime) { '1970-01-01' }
    let!(:real_datetime) { DateTimeHelper.convert_unixtime_to_date(0) }
    context 'when convert unixtime from integer format to date %Y-%m-%d' do
      it "returns the beginning of the unix history" do
        expect(real_datetime).to eq(mock_datetime)
      end
    end
  end
end