require './incident'

describe Incident do
  describe ".initialize" do
    let!(:incident) {Incident.new('Test incident', '2022-01-01')}
    context "given a creation date when is created" do
      it "return the same date as created_at" do
        expect(incident.created_at).to eq('2022-01-01')
      end
    end
    context "given a description when is created" do
      it "return the same description as description" do
        expect(incident.description).to eq('Test incident')
      end
    end
    context "given a new instance when is created" do
      it "return solved_at as nil" do
        expect(incident.solved_at).to be(nil)
      end
    end
  end

  describe ".mark_as_solved" do
    let!(:solved_at) {Incident.new('Test incident', '2022-01-01').mark_as_solved}
    let!(:unsolved) {Incident.new('Test incident', '2022-01-01')}
    context "given a solution date when is marked as solved" do
      it "return a solution date" do
        expect(solved_at).not_to be(nil)
      end
    end
  end
end