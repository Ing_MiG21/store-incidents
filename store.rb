class Store
  attr_reader :incidents

  def initialize(incidents = [])
    @incidents = incidents
  end

  def incident_status(start_date, end_date)
    calculate_stats(start_date, end_date)
  end

  private

  def set_stats
    {
      :open_cases=> 0,
      :closed_cases => 0,
      :average_solution => 0,
      :maximum_solution => 0,
    }
  end

  def filtered_cases
    {
      :open_cases => nil,
      :closed_cases => nil,
    }
  end

  def filter_incidents_by_date_and_status(start_date, end_date, status = :open)
    start_date, end_date = [DateTimeHelper.convert_date_to_unixtime(start_date), DateTimeHelper.convert_date_to_unixtime(end_date)]
    raise(RuntimeError, 'The start date cannot be higher than end date') if start_date > end_date

    self.incidents.filter do |incident|
      incident_created_at = DateTimeHelper.convert_date_to_unixtime(incident.created_at)
      incident_created_at >= start_date && incident_created_at <= end_date && incident.status == status
    end
  end

  def calculate_stats(start_date, end_date)
    stats = set_stats
    cases = filtered_cases

    {:open => :open_cases, :solved => :closed_cases}.each do |key, value|
      cases[value] = filter_incidents_by_date_and_status(start_date, end_date, key)
      stats[value] = cases[value].count
    end

    total_unit_time_cases = cases[:closed_cases].reduce(0){|sum, incident| sum + DateTimeHelper.calculate_time_between_dates(incident.created_at, incident.solved_at)}

    stats[:average_solution] = stats[:closed_cases] > 0 ? total_unit_time_cases / cases[:closed_cases].count : 0
    max_sol_incident = cases[:closed_cases].sort_by{|incident| DateTimeHelper.calculate_time_between_dates(incident.created_at, incident.solved_at)}.last
    stats[:maximum_solution] = DateTimeHelper.calculate_time_between_dates(max_sol_incident.created_at, max_sol_incident.solved_at) || 0
    stats
  end
end