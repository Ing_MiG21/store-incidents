require 'date'

module DateTimeHelper

  HOUR_IN_SECONDS = 3600
  DAY_IN_SECONDS = 86400
  DAY_IN_HOURS = DAY_IN_SECONDS / HOUR_IN_SECONDS

  def DateTimeHelper.convert_date_to_unixtime(input_date)
    Date.parse(input_date).to_time.to_i
  end

  def DateTimeHelper.calculate_time_between_dates(start_date, end_date = Time.now, convert_into = 'days')
    case convert_into
    when 'hours'
      (convert_date_to_unixtime(end_date) - convert_date_to_unixtime(start_date)) / HOUR_IN_SECONDS
    when 'seconds'
      convert_date_to_unixtime(end_date) - convert_date_to_unixtime(start_date)
    else 'days'
      (convert_date_to_unixtime(end_date) - convert_date_to_unixtime(start_date)) / DAY_IN_SECONDS
    end
  end

  def DateTimeHelper.convert_unixtime_to_date(unixtime)
     DateTime.strptime(unixtime.to_s,'%s').strftime("%Y-%m-%d")
  end

end