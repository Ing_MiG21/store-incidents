require 'pry'
require 'optparse'
require './datetime_helper.rb'
require './incident.rb'
require './store.rb'


params = {}
OptionParser.new do |opts|
  opts.banner = "Usage: start.rb [options]"

  opts.on('-f', '--from F', Integer, "From") {|f| params[:from] = f}
  opts.on('-t', '--to T', Integer, "To") {|t| params[:to] = t}
  opts.on('-s', '--start_date SD', String, "Start Date") {|s| params[:start_date] = s}
  opts.on('-e', '--end_date ED', String, "End Date") {|e| params[:end_date] = e}
end.parse!

incidents = params[:from].upto(params[:to]).map do |id|
  st_date = DateTimeHelper.convert_date_to_unixtime(params[:start_date])
  factor = ((DateTimeHelper::DAY_IN_SECONDS * id) + DateTimeHelper::DAY_IN_SECONDS)
  st_date += factor
  # puts DateTimeHelper.convert_unixtime_to_date(st_date)
  Incident.new("Incident_#{id}", DateTimeHelper.convert_unixtime_to_date(st_date))
end

incidents[1..33].each(&:mark_as_solved)

store = Store.new(incidents)

end_date = params[:end_date] || Time.now.strftime("%Y-%m-%d")
puts store.incident_status(params[:start_date], end_date)