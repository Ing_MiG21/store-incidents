## Bienvenidos

Hola a todos por acá. Este pequeño problema fue solventado en Ruby versión 2.6.5

### Ejecución de este ejercicio

```bash
# Empecemos instalando las gemas correspondientes

bundle install # recuerden tener instalado bundle => gem bundle

Para ejecutar los tests

bundle exec rspec # ejecutará todos los tests definidos.

# Si desean ejecutar un archivo con datos de prueba

# ejecutable con ruby

ruby start.rb -f 1 -t 100 -s '2021-01-01' -e '2021-12-31'

# o también

ruby start.rb --from 1 --to 100 --start_date '2021-01-01' --end_date '2021-12-31'

# los parámetros --from --to indican cuantos incidentes desean crearse antes de ser introducidos en la tienda

# les mostrará los resultados de un ejercicio y pueden ir jugando con los datos para ir viendo la salida y evaluarla

```