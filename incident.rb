class Incident

  attr_accessor :status, :description, :created_at, :solved_at

  def initialize(description, created_at = nil)
    @description = description
    @status = :open
    @created_at = created_at || Time.now.strftime("%Y-%m-%d")
    @solved_at = nil
  end

  def mark_as_solved(solved_at = nil)
    @status = :solved
    @solved_at = solved_at || Time.now.strftime("%Y-%m-%d")
  end
end